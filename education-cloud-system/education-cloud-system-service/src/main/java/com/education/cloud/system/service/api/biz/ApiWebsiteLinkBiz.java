package com.education.cloud.system.service.api.biz;

import java.util.List;

import com.education.cloud.system.common.dto.WebsiteLinkDTO;
import com.education.cloud.system.common.dto.WebsiteLinkListDTO;
import com.education.cloud.system.service.dao.WebsiteLinkDao;
import com.education.cloud.util.base.PageUtil;
import com.education.cloud.util.base.Result;
import com.education.cloud.util.enums.StatusIdEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.education.cloud.system.service.dao.impl.mapper.entity.WebsiteLink;

/**
 * 站点友情链接
 *
 * @author wuyun
 */
@Component
public class ApiWebsiteLinkBiz {

	@Autowired
	private WebsiteLinkDao dao;

	public Result<WebsiteLinkListDTO> list() {
		List<WebsiteLink> websiteLinkList = dao.listByStatusId(StatusIdEnum.YES.getCode());
		WebsiteLinkListDTO dto = new WebsiteLinkListDTO();
		dto.setWebsiteLinkList(PageUtil.copyList(websiteLinkList, WebsiteLinkDTO.class));
		return Result.success(dto);
	}

}
