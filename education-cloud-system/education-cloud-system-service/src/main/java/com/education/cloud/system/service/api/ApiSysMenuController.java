package com.education.cloud.system.service.api;

import com.education.cloud.util.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.education.cloud.system.service.api.biz.ApiSysMenuBiz;

/**
 * 菜单信息
 *
 * @author wujing
 */
@RestController
public class ApiSysMenuController extends BaseController {

	@Autowired
	private ApiSysMenuBiz biz;

}
