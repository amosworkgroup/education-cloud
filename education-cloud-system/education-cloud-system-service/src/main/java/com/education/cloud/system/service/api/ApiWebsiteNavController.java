package com.education.cloud.system.service.api;

import com.education.cloud.system.common.dto.WebsiteNavListDTO;
import com.education.cloud.util.base.BaseController;
import com.education.cloud.util.base.Result;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.education.cloud.system.service.api.biz.ApiWebsiteNavBiz;
import io.swagger.annotations.ApiOperation;

/**
 * 站点导航
 *
 * @author wuyun
 */
@Api(value = "站点导航", tags = "站点导航")
@RestController
public class ApiWebsiteNavController extends BaseController {

	@Autowired
	private ApiWebsiteNavBiz biz;

	/**
	 * 获取站点导航信息接口
	 *
	 * @return
	 * @author wuyun
	 */
	@ApiOperation(value = "获取站点导航信息接口", notes = "获取站点导航信息")
	@RequestMapping(value = "/system/api/website/nav/list", method = RequestMethod.POST)
	public Result<WebsiteNavListDTO> list() {
		return biz.list();
	}

}
