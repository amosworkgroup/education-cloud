package com.education.cloud.util.annotation;

import com.education.cloud.util.config.ServerProtectConfig;
import com.education.cloud.util.config.ServerProtectConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Description TODO
 * @Date 2020/3/22
 * @Created by 67068
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(ServerProtectConfig.class)
public @interface EnableServerProtect {
}
